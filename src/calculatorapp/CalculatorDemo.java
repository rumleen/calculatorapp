/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package calculatorapp;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class CalculatorDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AdditionCalculator additionCalculator = new AdditionCalculator(15, 10);
        SubtractionCalculator subtractionCalculator = new SubtractionCalculator(15, 10);
        calculate(additionCalculator);
        calculate(subtractionCalculator);
    }

    public static void calculate(Calculator calculator) {
        System.out.println(calculator.calculate());
    }
    
}
