/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package calculatorapp;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class AdditionCalculator extends Calculator {
    public AdditionCalculator(double operand1, double operand2) {
        super(operand1, operand2);
    }

    @Override
    public double calculate() {
        return super.operand1 + super.operand2;
    }

}
